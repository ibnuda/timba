module Request.LihatPetugas exposing (..)

import Data.AuthToken exposing (AuthToken, withAuthorization)
import Data.Petugas as Petugas exposing (..)
import Http
import HttpBuilder exposing (RequestBuilder, withBody, withExpect, withQueryParams)
import Json.Encode as Encode
import Request.Bantuan exposing (apiUrl)
import Util exposing ((=>))


getDaftarPetugas : Maybe AuthToken -> Http.Request (List Petugas)
getDaftarPetugas mtoken =
    let
        ekspektasi =
            Petugas.listDecoder
                |> Http.expectJson
    in
    apiUrl "/petugas"
        |> HttpBuilder.get
        |> HttpBuilder.withExpect ekspektasi
        |> withAuthorization mtoken
        |> HttpBuilder.toRequest


postTambahPetugas : Maybe AuthToken -> { a | nama : String, password : String, telepon : String } -> Http.Request Petugas
postTambahPetugas mtoken { nama, telepon, password } =
    let
        petugas =
            Encode.object
                [ "nama" => Encode.string nama
                , "telepon" => Encode.string telepon
                , "password" => Encode.string password
                ]

        ekspektasi =
            Petugas.decoder |> Http.expectJson
    in
    apiUrl "/petugas"
        |> HttpBuilder.post
        |> HttpBuilder.withExpect ekspektasi
        |> HttpBuilder.withJsonBody petugas
        |> withAuthorization mtoken
        |> HttpBuilder.toRequest
