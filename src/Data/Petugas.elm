module Data.Petugas exposing (..)

import Json.Decode as Decode
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (decode, required)
import Json.Encode as Encode exposing (Value)
import Util exposing ((=>))


type alias RequestPetugas =
    { nama : String
    , telepon : String
    , password : String
    }


type alias Petugas =
    { nama : String
    , telepon : String
    , kode : String
    }


decoder : Decoder Petugas
decoder =
    decode Petugas
        |> required "nama" Decode.string
        |> required "telepon" Decode.string
        |> required "kode" Decode.string


encode : RequestPetugas -> Value
encode petugas =
    Encode.object
        [ "nama" => Encode.string petugas.nama
        , "telepon" => Encode.string petugas.telepon
        , "password" => Encode.string petugas.password
        ]


listDecoder : Decoder (List Petugas)
listDecoder =
    Decode.list decoder
