module Laman.DaftarPetugas exposing (..)

import DaftarRute as Rute
import Data.Petugas as Petugas
import Data.Sesi as Sesi
import Html exposing (..)
import Html.Attributes exposing (..)
import Http as Http
import Json.Decode as Decode
import Laman.GagalMuat as GagalMuat
import Request.LihatPetugas as LihatPetugas
import Task exposing (Task)
import Util exposing ((=>), penangangalat)


type alias Model =
    { galat : String
    , daftarpetugas : List Petugas.Petugas
    }

init : Sesi.Sesi -> Task GagalMuat.LamanGagalDimuat Model
init sesi =
    let
        mtoken =
            Maybe.map .token sesi.pengguna

        daftarpetugas =
            LihatPetugas.getDaftarPetugas mtoken
                |> Http.toTask

        gagalpenangan =
            penangangalat
                >> GagalMuat.lamanGagalDimuat
    in
    Task.map (Model "") daftarpetugas
        |> Task.mapError gagalpenangan

view : Sesi.Sesi -> Model -> Html msg
view _ model =
    div [ class "content" ]
        [ h1 [ class "header" ]
            [ p [ class "header" ]
                [ text "Daftar Petugas"
                , a [ Rute.href Rute.TambahPetugas, class "button is-pulled-right is-primary" ]
                    [ text "Tambah Petugas" ]
                ]
            ]
        , div [ class "section" ]
            [ table [ class "table is-stripped is-fullwidth table-container" ]
                [ thead [ class "thead" ]
                    [ tr [ class "tr" ]
                        [ th [ class "th" ] [ text "Nama" ]
                        , th [ class "th" ] [ text "Nomor Telepon" ]
                        , th [ class "th" ] [ text "Nomor Petugas" ]
                        ]
                    ]
                , tbody [ class "tbody" ] <| List.map viewdatapetugas model.daftarpetugas
                ]
            ]
        ]



viewdatapetugas : Petugas.Petugas -> Html msg
viewdatapetugas petugas =
    tr [ class "tr" ]
        [ td [ class "td" ] [ text petugas.nama ]
        , td [ class "td" ] [ text petugas.telepon ]
        , td [ class "td" ] [ text petugas.kode ]
        ]


type Msg
    = NoOp
    | DaftarPetugasTerunduh (Result Http.Error (List Petugas.Petugas))


update : Sesi.Sesi -> Msg -> Model -> ( Model, Cmd Msg )
update sesi msg model =
    case msg of
        NoOp ->
            model => Cmd.none

        DaftarPetugasTerunduh (Err g) ->
            let
                pesangalat =
                    penangangalat g
            in
            { model | galat = pesangalat } => Cmd.none

        DaftarPetugasTerunduh (Ok dp) ->
            { model | daftarpetugas = dp } => Cmd.none
