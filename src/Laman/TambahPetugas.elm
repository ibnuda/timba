module Laman.TambahPetugas exposing (..)

import DaftarRute as Rute
import Data.Petugas as Petugas
import Data.Sesi as Sesi
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http as Http
import Request.LihatPetugas as LihatPetugas
import Util exposing ((=>), penangangalat)
import Validate exposing (Validator, ifBlank, ifNotInt, validate)
import Views.Borang as Borang


type alias Model =
    { galat : List String
    , nama : String
    , telepon : String
    , password : String
    }


initmodel : Model
initmodel =
    { galat = []
    , nama = ""
    , telepon = ""
    , password = ""
    }


type Msg
    = AjukanBorang
    | SetNamaPetugas String
    | SetTeleponPetugas String
    | SetPassword String
    | TambahPetugasSelesai (Result Http.Error Petugas.Petugas)


validator : Validator String { a | nama : String, password : String, telepon : String }
validator =
    Validate.all
        [ ifBlank .nama "Nama petugas tidak boleh kosong."
        , ifBlank .telepon "Nomor telepon harap diisi."
        , ifBlank .password "Password tidak boleh kosong."
        ]


update : Sesi.Sesi -> Msg -> Model -> ( Model, Cmd Msg )
update sesi msg model =
    case msg of
        SetNamaPetugas s ->
            { model | nama = s }
                => Cmd.none
        SetTeleponPetugas s ->
            { model | telepon = s } => Cmd.none
        SetPassword s ->
            { model | password = s } => Cmd.none

        AjukanBorang ->
            case validate validator model of
                [] ->
                    { model | galat = [] }
                        => Http.send TambahPetugasSelesai (kirimPetugas sesi model)

                x ->
                    { model | galat = x }
                        => Cmd.none

        TambahPetugasSelesai (Ok r) ->
            model
                => Rute.modifikasiUrl Rute.DaftarPetugas

        TambahPetugasSelesai (Err g) ->
            let
                pesangalat =
                    penangangalat g
            in
            { model | galat = [ pesangalat ] }
                => Cmd.none

kirimPetugas : Sesi.Sesi -> Model -> Http.Request Petugas.Petugas
kirimPetugas sesi model =
    let
        mtoken =
            Maybe.map .token sesi.pengguna
    in
    LihatPetugas.postTambahPetugas mtoken model

view : Sesi.Sesi -> Model -> Html Msg
view sesi model =
    div [ class "content" ]
        [ div [ class "columns" ]
            [ div [ class "column is-one-fifth" ]
                []
            , div [ class "column auto" ]
                [ h2 [ class "header" ] [ text "Tambah Petugas" ]
                , Borang.viewGalat model.galat
                , viewborangpetugasbaru
                ]
            , div [ class "column is-one-fifth" ]
                []
            ]
        ]

viewborangpetugasbaru : Html Msg
viewborangpetugasbaru =
    Html.form [ onSubmit AjukanBorang ]
        [ Borang.input
            [ class "input"
            , onInput SetNamaPetugas
            , placeholder "Nama Petugas"
            ]
            []
        , Borang.input
            [ class "input"
            , onInput SetTeleponPetugas
            , placeholder "Nomor Telepon"
            ]
            []
        , Borang.password
            [ class "input"
            , onInput SetPassword
            , placeholder "Password Petugas"
            ]
            []
        , button [ class "button is-primary is-pulled-right" ]
            [ text "Tambah"
            ]
        ]
